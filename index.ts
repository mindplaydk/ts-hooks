/// This interface defines an event listener
export interface IListener<Event> {
    (event: Event): void
}

/// This interface represents an event hook
export interface IHook<Event> {
    /// Attach a handler to this hookable
    (handler: IListener<Event>): void

    /// Send an event to all listeners
    (event: Event): void
    
    or<Other>(other: IHook<Other>) : IHook<Event|Other>
}

/// This interface represents a hookable boxed value
export interface IBox<TValue> extends IHook<TValue> {
    /// Retrieve the value
    () : TValue
}

function return_null() {
	return null
}

/// Create an event hook for a specific type of message
function create_hook<Event>(getter? : { (): Event }): IHook<Event> {
    var _handlers: Array<Function> = []
    var _busy = false
    var _getter = getter || return_null
    
    function invoke(message)
    function invoke(event: Event)
    function invoke() {
        if (arguments.length === 0) {
            return _getter()
        } else if (arguments[0] instanceof Function) {
            _handlers.push(arguments[0])
        } else if (!_busy) {
            _busy = true
            for (var i = 0; i < _handlers.length; i++) {
                _handlers[i].apply(this, arguments)
            }
            _busy = false
        }
    }
    
    invoke["or"] = function <Other> (other: IHook<Other>) : IHook<Event|Other> {
        return <IHook<Event>> function (event: Event|Other) {
            invoke(<Event> event)
            other(<Other> event)
        }
    }
    
    return <IHook<Event>> <any> invoke
}

/// Create an event hook for a specific type of message
export var hook = <{ (): IHook<Event> }> create_hook

/// Create a hookable boxed value (optionally with an initial value)
export function box<T>(value?: T): IBox<T> {
    var _value: T = value
    var _hook = create_hook(() => _value)
    
    _hook((value: T) => _value = value)
    
    return <IBox<T>> _hook;
}
